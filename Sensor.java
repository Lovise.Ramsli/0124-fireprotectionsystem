/**
 * En abstraksjon av en fysisk sensor. Er ansvarlig for kommunikasjon
 * med en eventuell fysisk sensor; i vårt tilfelle gjør vi istedet
 * tilgjengelig en metode <code>setSmokeDetected</code> for testing.
 */
public class Sensor {
    boolean smokeDetected;
    FireProtectionSystem system;

    /**
     * Setter hvorvidt røyk er oppdaget.
     * @param smokeDetected
     */
    public void setSmokeDetected(boolean smokeDetected) {
        this.smokeDetected = smokeDetected;
        if (smokeDetected) {
            system.smokeDetected();
        }
        else {
            system.stoppedDetectingSmoke();
        }
    }

    /**
     * Installer et brannvarslingssystem denne sensoren rapporterer til
     * når den oppdager røyk.
     * @param system
     */
    void installSystem(FireProtectionSystem system) {
        this.system = system;
    }

}
